# Support Driven Department Survey

We’ve seen the results of what can happen when we share data from the Support Driven Salary Survey. It’s led to people making a stronger case for and getting raises, negotiating starting salaries that are well above what they initially thought, and getting a better understanding of the job marketplace.

The Support Driven Department Survey will help you answer questions about budgets so you can make a case for requesting more resources or show how you’re effectively and efficiently working with the resources you have. We also have questions related to outsourcing strategies, support channels, professional development, and more that can inform and shape the stories around those decisions.

We’ve made an impact for a lot of people on a personal level, and now we have an opportunity to do so at the department level.

## How you can help
1. Share your experiences with creating budgets, making the case for resources, working with other departments, and advocating for change with the community. To do so, please get in touch with @scott in the Support Driven Slack.
1. Give us feedback and suggest improvements to the questions in the survey by [filing an issue](https://gitlab.com/support-driven/support-driven-department-survey/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=).
